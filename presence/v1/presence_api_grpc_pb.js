// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var presence_v1_presence_api_pb = require('../../presence/v1/presence_api_pb.js');
var presence_v1_channel_pb = require('../../presence/v1/channel_pb.js');

function serialize_localticketing_presence_v1_GetChannelRequest(arg) {
  if (!(arg instanceof presence_v1_presence_api_pb.GetChannelRequest)) {
    throw new Error('Expected argument of type localticketing.presence.v1.GetChannelRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_localticketing_presence_v1_GetChannelRequest(buffer_arg) {
  return presence_v1_presence_api_pb.GetChannelRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_localticketing_presence_v1_GetChannelResponse(arg) {
  if (!(arg instanceof presence_v1_presence_api_pb.GetChannelResponse)) {
    throw new Error('Expected argument of type localticketing.presence.v1.GetChannelResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_localticketing_presence_v1_GetChannelResponse(buffer_arg) {
  return presence_v1_presence_api_pb.GetChannelResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_localticketing_presence_v1_ListChannelsRequest(arg) {
  if (!(arg instanceof presence_v1_presence_api_pb.ListChannelsRequest)) {
    throw new Error('Expected argument of type localticketing.presence.v1.ListChannelsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_localticketing_presence_v1_ListChannelsRequest(buffer_arg) {
  return presence_v1_presence_api_pb.ListChannelsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_localticketing_presence_v1_ListChannelsResponse(arg) {
  if (!(arg instanceof presence_v1_presence_api_pb.ListChannelsResponse)) {
    throw new Error('Expected argument of type localticketing.presence.v1.ListChannelsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_localticketing_presence_v1_ListChannelsResponse(buffer_arg) {
  return presence_v1_presence_api_pb.ListChannelsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


// Handles interaction with presence api.
var PresenceAPIService = exports.PresenceAPIService = {
  // Get channel specified by id.
  getChannel: {
    path: '/localticketing.presence.v1.PresenceAPI/GetChannel',
    requestStream: false,
    responseStream: false,
    requestType: presence_v1_presence_api_pb.GetChannelRequest,
    responseType: presence_v1_presence_api_pb.GetChannelResponse,
    requestSerialize: serialize_localticketing_presence_v1_GetChannelRequest,
    requestDeserialize: deserialize_localticketing_presence_v1_GetChannelRequest,
    responseSerialize: serialize_localticketing_presence_v1_GetChannelResponse,
    responseDeserialize: deserialize_localticketing_presence_v1_GetChannelResponse,
  },
  // List all channels.
  listChannels: {
    path: '/localticketing.presence.v1.PresenceAPI/ListChannels',
    requestStream: false,
    responseStream: false,
    requestType: presence_v1_presence_api_pb.ListChannelsRequest,
    responseType: presence_v1_presence_api_pb.ListChannelsResponse,
    requestSerialize: serialize_localticketing_presence_v1_ListChannelsRequest,
    requestDeserialize: deserialize_localticketing_presence_v1_ListChannelsRequest,
    responseSerialize: serialize_localticketing_presence_v1_ListChannelsResponse,
    responseDeserialize: deserialize_localticketing_presence_v1_ListChannelsResponse,
  },
};

exports.PresenceAPIClient = grpc.makeGenericClientConstructor(PresenceAPIService);
