/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.localticketing.ticketcreator.v1.PrintType', null, global);
/**
 * @enum {number}
 */
proto.localticketing.ticketcreator.v1.PrintType = {
  PRINT_TYPE_INVALID: 0,
  PRINT_TYPE_HARDTICKET_A4: 1,
  PRINT_TYPE_ONLINETICKET_A4: 2,
  PRINT_TYPE_PKPASS: 3
};

goog.object.extend(exports, proto.localticketing.ticketcreator.v1);
