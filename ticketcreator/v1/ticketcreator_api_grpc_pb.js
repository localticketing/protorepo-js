// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var ticketcreator_v1_ticketcreator_api_pb = require('../../ticketcreator/v1/ticketcreator_api_pb.js');
var ticketcreator_v1_ticket_pb = require('../../ticketcreator/v1/ticket_pb.js');
var ticketcreator_v1_print_type_pb = require('../../ticketcreator/v1/print_type_pb.js');

function serialize_localticketing_ticketcreator_v1_GenerateTicketsRequest(arg) {
  if (!(arg instanceof ticketcreator_v1_ticketcreator_api_pb.GenerateTicketsRequest)) {
    throw new Error('Expected argument of type localticketing.ticketcreator.v1.GenerateTicketsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_localticketing_ticketcreator_v1_GenerateTicketsRequest(buffer_arg) {
  return ticketcreator_v1_ticketcreator_api_pb.GenerateTicketsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_localticketing_ticketcreator_v1_GenerateTicketsResponse(arg) {
  if (!(arg instanceof ticketcreator_v1_ticketcreator_api_pb.GenerateTicketsResponse)) {
    throw new Error('Expected argument of type localticketing.ticketcreator.v1.GenerateTicketsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_localticketing_ticketcreator_v1_GenerateTicketsResponse(buffer_arg) {
  return ticketcreator_v1_ticketcreator_api_pb.GenerateTicketsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


// Ticketcreator api to generate tickets into pdfs, or pkpass.
var TicketcreatorAPIService = exports.TicketcreatorAPIService = {
  // Generate Tickets gives back a url to the respective tickets.
  generateTickets: {
    path: '/localticketing.ticketcreator.v1.TicketcreatorAPI/GenerateTickets',
    requestStream: false,
    responseStream: false,
    requestType: ticketcreator_v1_ticketcreator_api_pb.GenerateTicketsRequest,
    responseType: ticketcreator_v1_ticketcreator_api_pb.GenerateTicketsResponse,
    requestSerialize: serialize_localticketing_ticketcreator_v1_GenerateTicketsRequest,
    requestDeserialize: deserialize_localticketing_ticketcreator_v1_GenerateTicketsRequest,
    responseSerialize: serialize_localticketing_ticketcreator_v1_GenerateTicketsResponse,
    responseDeserialize: deserialize_localticketing_ticketcreator_v1_GenerateTicketsResponse,
  },
};

exports.TicketcreatorAPIClient = grpc.makeGenericClientConstructor(TicketcreatorAPIService);
